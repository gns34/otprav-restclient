package ru.ztech.otprav.rest.test;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import okhttp3.OkHttpClient;
import ru.gns34.restclient.AuthenticationPlainInterceptor;
import ru.gns34.restclient.RestException;
import ru.ztech.otprav.ITaskService;
import ru.ztech.otprav.api.Contact;
import ru.ztech.otprav.api.Task;
import ru.ztech.otprav.rest.Restclient;
import ru.ztech.otprav.rest.TaskServiceOkHttpImpl;

/**
 * TaskServiceTest
 *
 * @since version(20-06-2019)
 * @author Гончаров Никита 
 * 
 */
public class TaskServiceTest {
    
    // ~ Статические поля/инициализации =====================================================================
    
    // ~ Переменные(свойства) класса ========================================================================
    private static ITaskService taskService;
    private static ITaskService taskServiceAuth;
    
    // ~ Управляемые объекты(Beans) =========================================================================
    
    // ~ Конструктор ========================================================================================
    @BeforeClass
    public static void config() {
        final OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.readTimeout(60, TimeUnit.MINUTES);
        String host = "http://otprav.ru/api/";
        taskService = new TaskServiceOkHttpImpl(new Restclient(builder.build(), host));
        
        builder.addInterceptor(new AuthenticationPlainInterceptor("test1@otprav.ru", "13579"));
        taskServiceAuth = new TaskServiceOkHttpImpl(new Restclient(builder.build(), host));
    }
    
    // ~ Методы =============================================================================================
    @Test
    public void testTaskById() throws RestException {
        try {
            taskService.getTaskById("5");
        } catch (RestException e) {
            if (!RestException.CODE_INTERNAL_ERROR.equals(e.getCode())) {
                Assert.fail();
            }
        }
        Task taskById = taskServiceAuth.getTaskById("5");
        System.out.println("D " + taskById);
    }
    
    @Test
    public void tesTasksByCountry() throws RestException {
        List<Task> country = taskService.getTasksByCountry("Россия");
        System.out.println("C " + country);
    }
    
    @Test
    public void tesContactByTaskId() throws RestException {
        Contact contact = taskService.getContactByTaskId("d");
        System.out.println("C " + contact);
    }
    
    @Test
    public void tesTasksByMe() throws RestException {
        try {
            taskService.getTasksByMe();
        } catch (RestException e) {
            if (!RestException.CODE_INTERNAL_ERROR.equals(e.getCode())) {
                Assert.fail();
            }
        }
        List<Task> tasks = taskServiceAuth.getTasksByMe();
        System.out.println("T " + tasks);
    }
    
    @Test
    public void tesTasksByActiveMe() throws RestException {
        try {
            taskService.getTasksByActiveMe();
        } catch (RestException e) {
            if (!RestException.CODE_INTERNAL_ERROR.equals(e.getCode())) {
                Assert.fail();
            }
        }
        List<Task> tasks = taskServiceAuth.getTasksByActiveMe();
        System.out.println("Task " + tasks);
    }
    
    @Test
    public void tesSaveTask() throws RestException {
        final Task task = new Task();
        task.setAdress("after city");
        task.setCity("A" + UUID.randomUUID().toString());
        task.setCoord("ffff");
        task.setCountry("myau");
        task.setCreateStamp("26.06.2020");
        task.setPrice("111,13");
        task.setRegion("MU");
        task.setTexttask("" + UUID.randomUUID().toString());
        task.setText("ddsadasds");
        try {
            taskService.saveTask(task);
        } catch (RestException e) {
            if (!RestException.CODE_INTERNAL_ERROR.equals(e.getCode())) {
                Assert.fail();
            }
        }
        taskServiceAuth.saveTask(task);
        System.out.println("Task save otlihno");
    }
}
