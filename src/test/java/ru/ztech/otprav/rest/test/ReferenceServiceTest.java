package ru.ztech.otprav.rest.test;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.BeforeClass;
import org.junit.Test;

import okhttp3.OkHttpClient;
import ru.gns34.restclient.RestException;
import ru.ztech.otprav.IReferenceService;
import ru.ztech.otprav.api.City;
import ru.ztech.otprav.api.Country;
import ru.ztech.otprav.api.Region;
import ru.ztech.otprav.api.Status;
import ru.ztech.otprav.rest.ReferenceServiceOkHttpImpl;
import ru.ztech.otprav.rest.Restclient;

/**
 * ReferenceServiceTest
 *
 * @since version(20-06-2019)
 * @author Гончаров Никита 
 * 
 */
public class ReferenceServiceTest {
    
    // ~ Статические поля/инициализации =====================================================================
    
    // ~ Переменные(свойства) класса ========================================================================
    private static IReferenceService referenceService;
    // ~ Управляемые объекты(Beans) =========================================================================
    
    // ~ Конструктор ========================================================================================
    @BeforeClass
    public static void config() {
        final OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.readTimeout(60, TimeUnit.MINUTES);
        String host = "http://otprav.ru/api/";
        referenceService = new ReferenceServiceOkHttpImpl(new Restclient(builder.build(), host));
    }
    
    // ~ Методы =============================================================================================
    @Test
    public void tesRegion() throws RestException {
        List<Region> region = referenceService.getRegion("Россия");
        System.out.println("Region " + region);
    }
    
    @Test
    public void testCity() throws RestException {
        List<City> city = referenceService.getCity("Россия", "Московская область");
        System.out.println("City " + city);
    }
    
    @Test
    public void tesCountry() throws RestException {
        List<Country> country = referenceService.getCountry();
        System.out.println("Country " + country);
    }
    
    @Test
    public void tesStatus() throws RestException {
        List<Status> status = referenceService.getStatus();
        System.out.println("Status " + status);
    }
}
