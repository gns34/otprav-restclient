package ru.ztech.otprav;

import java.util.List;

import ru.gns34.restclient.RestException;
import ru.ztech.otprav.api.Contact;
import ru.ztech.otprav.api.Task;

/**
 * ITaskService
 *
 * @since version(20-06-2019)
 * @author Гончаров Никита 
 * 
 */
public interface ITaskService {
    
    List<Task> getTasksByCountry(String сountry) throws RestException;
    
    Task getTaskById(String id) throws RestException;
    
    Contact getContactByTaskId(String id) throws RestException;
    
    List<Task> getTasksByMe() throws RestException;
    
    List<Task> getTasksByActiveMe() throws RestException;

    void saveTask(Task task) throws RestException;
}
