package ru.ztech.otprav.api;

/**
 * Status
 *
 * @since version(23-06-2019)
 * @author Гончаров Никита 
 * 
 */
public class Status {
    
    // ~ Статические поля/инициализации =====================================================================
    
    // ~ Переменные(свойства) класса ========================================================================
    private String status;
    private String statusname;
    // ~ Управляемые объекты(Beans) =========================================================================
    
    // ~ Конструктор ========================================================================================
    
    // ~ Методы =============================================================================================
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Status [status=" + status + ", statusname=" + statusname + "]";
    }
    
    // ~ Доступ к свойствам =================================================================================
    /**
     * @return status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status - Установить status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return statusname
     */
    public String getStatusname() {
        return statusname;
    }

    /**
     * @param statusname - Установить statusname
     */
    public void setStatusname(String statusname) {
        this.statusname = statusname;
    }
}
