package ru.ztech.otprav.api;

/**
 * Task
 *
 * @since version(20-06-2019)
 * @author Гончаров Никита 
 * 
 */
public class Task {
    
    // ~ Статические поля/инициализации =====================================================================
    
    // ~ Переменные(свойства) класса ========================================================================
    private String price;
    private String createStamp;
    private String region;
    private String country;
    private String city;
    private String coord;
    private String adress;
    private String texttask;
    private String text;
    
    // ~ Конструктор ========================================================================================
    
    // ~ Методы =============================================================================================
    
    // ~ Доступ к свойствам =================================================================================
    /**
     * @return price
     */
    public String getPrice() {
        return price;
    }
    /**
     * @param price - Установить price
     */
    public void setPrice(String price) {
        this.price = price;
    }
    /**
     * @return createStamp
     */
    public String getCreateStamp() {
        return createStamp;
    }
    /**
     * @param createStamp - Установить createStamp
     */
    public void setCreateStamp(String createStamp) {
        this.createStamp = createStamp;
    }
    /**
     * @return region
     */
    public String getRegion() {
        return region;
    }
    /**
     * @param region - Установить region
     */
    public void setRegion(String region) {
        this.region = region;
    }
    /**
     * @return country
     */
    public String getCountry() {
        return country;
    }
    /**
     * @param country - Установить country
     */
    public void setCountry(String country) {
        this.country = country;
    }
    /**
     * @return city
     */
    public String getCity() {
        return city;
    }
    /**
     * @param city - Установить city
     */
    public void setCity(String city) {
        this.city = city;
    }
    /**
     * @return coord
     */
    public String getCoord() {
        return coord;
    }
    /**
     * @param coord - Установить coord
     */
    public void setCoord(String coord) {
        this.coord = coord;
    }
    /**
     * @return adress
     */
    public String getAdress() {
        return adress;
    }
    /**
     * @param adress - Установить adress
     */
    public void setAdress(String adress) {
        this.adress = adress;
    }
    /**
     * @return texttask
     */
    public String getTexttask() {
        return texttask;
    }
    /**
     * @param texttask - Установить texttask
     */
    public void setTexttask(String texttask) {
        this.texttask = texttask;
    }
    /**
     * @return text
     */
    public String getText() {
        return text;
    }
    /**
     * @param text - Установить text
     */
    public void setText(String text) {
        this.text = text;
    }
}
