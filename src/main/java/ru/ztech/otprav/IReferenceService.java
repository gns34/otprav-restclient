package ru.ztech.otprav;

import java.util.List;

import ru.gns34.restclient.RestException;
import ru.ztech.otprav.api.City;
import ru.ztech.otprav.api.Country;
import ru.ztech.otprav.api.Region;
import ru.ztech.otprav.api.Status;

/**
 * IReferenceService
 *
 * @since version(20-06-2019)
 * @author Гончаров Никита 
 * 
 */
public interface IReferenceService {
    
    List<Country> getCountry() throws RestException;
    
    List<Region> getRegion(String country) throws RestException;
    
    List<City> getCity(String country, String region) throws RestException;
    
    List<Status> getStatus() throws RestException;
}
