package ru.ztech.otprav.rest;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.ResponseBody;
import ru.gns34.easy.support.util.UtilString;
import ru.gns34.restclient.AbscractOkHttpService;
import ru.gns34.restclient.OkHttpContainer;
import ru.gns34.restclient.RestException;
import ru.ztech.otprav.IReferenceService;
import ru.ztech.otprav.api.City;
import ru.ztech.otprav.api.Country;
import ru.ztech.otprav.api.Region;
import ru.ztech.otprav.api.Status;

/**
 * ReferenceServiceOkHttpImpl
 *
 * @since version(20-06-2019)
 * @author Гончаров Никита 
 * 
 */
public class ReferenceServiceOkHttpImpl extends AbscractOkHttpService implements IReferenceService {
    
    // ~ Статические поля/инициализации =====================================================================
    /**  */
    private static final String ACTION = "action";
    /**  */
    private static final String PARAM_COUNTRY = "country";
    /**  */
    private static final String PARAM_REGION = "region";
    
    // ~ Переменные(свойства) класса ========================================================================
    
    // ~ Управляемые объекты(Beans) =========================================================================
    
    // ~ Конструктор ========================================================================================
    public ReferenceServiceOkHttpImpl(OkHttpContainer container) {
        super(container);
    }

    // ~ Методы =============================================================================================
    /* (non-Javadoc)
     * @see ru.gns34.restclient.AbscractOkHttpService#getControllerSuffix()
     */
    @Override
    protected String getControllerSuffix() {
        return UtilString.BLANK;
    }

    /* (non-Javadoc)
     * @see ru.ztech.otprav.IReferenceService#getCountry()
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<Country> getCountry() throws RestException {
        final ResponseBody body = callServer(UtilString.BLANK, Collections.singletonMap(ACTION, "getcountry"));
        return extractObjects(body, List.class, Country.class);
    }

    /* (non-Javadoc)
     * @see ru.ztech.otprav.IReferenceService#getRegion()
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<Region> getRegion(String country) throws RestException {
        final Map<String, String> params = new HashMap<>();
        params.put(ACTION, "getregion");
        params.put(PARAM_COUNTRY, country);
        final ResponseBody body = callServer(UtilString.BLANK, params);
        return extractObjects(body, List.class, Region.class);
    }

    /* (non-Javadoc)
     * @see ru.ztech.otprav.IReferenceService#getCity()
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<City> getCity(String country, String region) throws RestException {
        final Map<String, String> params = new HashMap<>();
        params.put(ACTION, "getcity");
        params.put(PARAM_COUNTRY, country);
        params.put(PARAM_REGION, region);
        final ResponseBody body = callServer(UtilString.BLANK, Collections.singletonMap(ACTION, "getcity"));
        return extractObjects(body, List.class, City.class);
    }

    /* (non-Javadoc)
     * @see ru.ztech.otprav.IReferenceService#getStatus()
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<Status> getStatus() throws RestException {
        final ResponseBody body = callServer(UtilString.BLANK, Collections.singletonMap(ACTION, "statuslist"));
        return extractObjects(body, List.class, Status.class);
    }
    
    // ~ Доступ к свойствам =================================================================================
    
}
