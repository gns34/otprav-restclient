package ru.ztech.otprav.rest;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.ResponseBody;
import ru.gns34.easy.support.util.UtilString;
import ru.gns34.restclient.AbscractOkHttpService;
import ru.gns34.restclient.OkHttpContainer;
import ru.gns34.restclient.RestException;
import ru.ztech.otprav.ITaskService;
import ru.ztech.otprav.api.Contact;
import ru.ztech.otprav.api.Task;

/**
 * TaskServiceOkHttpImpl
 *
 * @since version(20-06-2019)
 * @author Гончаров Никита 
 * 
 */
public class TaskServiceOkHttpImpl extends AbscractOkHttpService implements ITaskService {

    
    // ~ Статические поля/инициализации =====================================================================
    /**  */
    private static final String ACTION = "action";
    /**  */
    private static final String PARAM_COUNTRY = "country";
    /**  */
    private static final String PARAM_TASK_ID = "idtask";
    
    // ~ Переменные(свойства) класса ========================================================================
    
    // ~ Управляемые объекты(Beans) =========================================================================
    
    // ~ Конструктор ========================================================================================
    public TaskServiceOkHttpImpl(OkHttpContainer container) {
        super(container);
    }
    
    // ~ Методы =============================================================================================
    /* (non-Javadoc)
     * @see ru.gns34.restclient.AbscractOkHttpService#getControllerSuffix()
     */
    @Override
    protected String getControllerSuffix() {
        return UtilString.BLANK;
    }

    /* (non-Javadoc)
     * @see ru.ztech.otprav.ITaskService#getTasksByCountry()
     */
    @Override
    public List<Task> getTasksByCountry(String сountry) throws RestException {
        final Map<String, String> params = new HashMap<>();
        params.put(ACTION, "getcity");
        params.put(PARAM_COUNTRY, сountry);
        final ResponseBody body = callServer(UtilString.BLANK, params);
        extractObject(body, Object.class);
        return null;
    }

    /* (non-Javadoc)
     * @see ru.ztech.otprav.ITaskService#getTaskById()
     */
    @Override
    public Task getTaskById(String id) throws RestException {
        final Map<String, String> params = new HashMap<>();
        params.put(ACTION, "getcontact");
        params.put(PARAM_TASK_ID, id);
        final ResponseBody body = callServer(UtilString.BLANK, params);
        extractObject(body, Object.class);
        return null;
    }

    /* (non-Javadoc)
     * @see ru.ztech.otprav.ITaskService#getContactByTaskId(java.lang.String)
     */
    @Override
    public Contact getContactByTaskId(String id) throws RestException {
        final Map<String, String> params = new HashMap<>();
        params.put(ACTION, "getcity");
        params.put(PARAM_TASK_ID, id);
        final ResponseBody body = callServer(UtilString.BLANK, params);
        extractObject(body, Object.class);
        return null;
    }

    /* (non-Javadoc)
     * @see ru.ztech.otprav.ITaskService#getTasksByMe(java.lang.String)
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<Task> getTasksByMe() throws RestException {
        final ResponseBody body = callServer(UtilString.BLANK, Collections.singletonMap(ACTION, "getmytask"));
        return extractObjects(body, List.class, Task.class);
    }

    /* (non-Javadoc)
     * @see ru.ztech.otprav.ITaskService#getTasksByActiveMe(java.lang.String)
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<Task> getTasksByActiveMe() throws RestException {
        final ResponseBody body = callServer(UtilString.BLANK, Collections.singletonMap(ACTION, "getmytaskex"));
        return extractObjects(body, List.class, Task.class);
    }

    /* (non-Javadoc)
     * @see ru.ztech.otprav.ITaskService#saveTask(ru.ztech.otprav.api.Task)
     */
    @Override
    public void saveTask(Task task) throws RestException {
        callServer("savetask", task);
    }
    
}
